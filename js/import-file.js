(function () {
  let app = angular.module('funifierImportApp', []);
  app.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
    $locationProvider.html5Mode({ enabled: true, requireBase: false });
  }]);
  app.controller('FunifierController', ['$scope', '$http', '$location', '$window', funifierController]);

  function funifierController($scope, $http, $location, $window) {
    $scope.token = "Basic NWI1ZjY3YmJmYWMxMjY0ZTQ2NDg4YmViOjViNWY2Yjk2ZmFjMTI2NGU0NjQ4OGUyYQ==";
    $scope.baseUrl = 'https://service2.funifier.com';
    // $scope.baseUrl = 'http://dev.service.funifier.com';
    $scope.selos = [];
    $scope.response = 0;
    $scope.listSelos = [];
    $scope.ListSelosDescription = [];
    $scope.showListSelos = false;
    


    $scope.getimportPage = function (id) {
      window.location.href = "import.html?id=" + id;
    };
    $scope.getPurchases = function () {
      window.location.href = "reportPurchases.html";
    };
    $scope.getIndex = function () {
      window.location.href = "index.html";
    };
    $scope.getPlayerPage = function () {
      window.location.href = "player.html";
    };
    $scope.getImportPlayerPage = function () {
      window.location.href = "import-player.html";
    };

    $scope.voltar = function () {
      window.location.href = "index.html?id=";
    };

    $scope.load = () => {
      let req = {
        method: 'GET',
        url: `${$scope.baseUrl}/v3/challenge`,
        headers: {
          "Authorization": $scope.token,
          "content-type": "application/json",
          "cache-control": "no-cache"
        }
      };
      $http(req).then(function (data) {
        $scope.ListSelosDescription = data.data;
        $scope.ListSelosDescription.filter((value) => {
          return $scope.listSelos.push(value._id);
        });
        // $scope.listSelos = data.data;
      }, function (err) {
        console.log(err);
      });
    }; // $scope.load
    $scope.load();

    $scope.setShow = () => {
      $scope.showListSelos === true ? $scope.showListSelos = false : $scope.showListSelos = true
    };

    $scope.uploadExcel = function () {
      $scope.spinner = true;
      $scope.fileSize = 0;
      $scope.filesTosubmit = [];
      $scope.filesWithError = [];

      let getFile = document.getElementById('file');
      let input = getFile;
      let reader = new FileReader();

      reader.onload = function () {
        let fileData = reader.result;
        let workbook = XLSX.read(fileData, { type: 'binary' });

        workbook.SheetNames.forEach(function (sheetName) {
          let rowObject = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          excelJsonObj = rowObject;
        });
        $scope.fileSize = excelJsonObj.length;

        let id = $location.search().id;
        for (let i = 0; i < excelJsonObj.length; i++) {
          let data = excelJsonObj[i];

          if (data.key && id) {
            actionTemp = {
              _id: data.key,
              actionId: "karbon_work",
              userId: data.assignee,
              attributes: {
                importId: id,
                title: data.title,
                client: data.client,
                type: data.type,
                status: data.status,
                start: data.start ? (new Date(data.start).getTime() / 1000) + "000" : null,
                due: data.due ? (new Date(data.due).getTime() / 1000) + "000" : null,
                completed: data.completed ? (new Date(data.completed).getTime() / 1000) + "000" : null,
                estimated_budget: data.estimated_budget,
                estimated_time: data.estimated_time,
                assignee: data.assignee,
                client_owner: data.client_owner,
                client_group: data.client_group
              }
            }
            $scope.filesTosubmit.push(actionTemp);
            $scope.spinner = false;
            console.log(actionTemp);
            // postFiles(actionTemp);
          } // if

        } // for

        $scope.$apply($scope.filesTosubmit);
        // console.log($scope.filesTosubmit);
      }; // reader.onload
      reader.readAsBinaryString(input.files[0]);

    }; // $scope.uploadExcel

    $scope.import = function () {
      $scope.spinner = true;
      if ($scope.filesWithError.length > 0) {
        if (confirm(`Please correct the ${$scope.filesWithError.length} errors.`)) {
          console.log('Import cancelled!');
        } else {
          console.log('Import cancelled!');
        }
      } else {
        if (confirm(`Do you want to continue importing ${$scope.filesTosubmit.length} actions?`)) {
          postFiles();
        } else {
          console.log('Import cancelled!');
        }
      } // if

      function postFiles() {
        let id = $location.search().id;
        let req1 = {
          method: 'DELETE',
          url: `${$scope.baseUrl}/v3/database?collection=action_log&q=attributes.importId:'${id}'`,
          headers: { "Authorization": $scope.token, "content-type": "application/json", "cache-control": "no-cache" }
        };
        $http(req1).then(function (data1) {
          let req = {
            method: 'POST',
            url: `${$scope.baseUrl}/v3/action/log/bulk`,
            headers: {
              "Authorization": $scope.token,
              "content-type": "application/json",
              "cache-control": "no-cache"
            },
            data: $scope.filesTosubmit
          };
          $http(req).then(function (data) {
            console.log('resposta:', data.data);
            if (data.data.content_with_not_allowed_players) {
              var playersWithErros= [];
              data.data.content_with_not_allowed_players.forEach(function(player) {
                playersWithErros.push(`Player: ${player.userId}, Title: ${player.attributes.title}` + '\n')
              });
            }
            $scope.dataResponse = data.data;
            // alert(`
            //   Total of actions imported: ${data.data.total_registered},
            //   Total of actions ignored: ${data.data.total_ignored},
            //   Content with not allowed players: 
            //   ${playersWithErros}
            // `)
            $('.modal').modal('show');
            $scope.spinner = false;
            // $scope.voltar();
          }, function (err) {
            console.log(err);
          });
        }, function (err) {
          console.log(err);
        });
      } // postFiles 

    }; // $scope.import

  } //funifierController
})();
