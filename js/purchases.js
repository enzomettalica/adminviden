(function () {
    let app = angular.module('funifierImportApp', []);
    app.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
        $locationProvider.html5Mode({ enabled: true, requireBase: false });
    }]);
    app.controller('FunifierController', ['$scope', '$http', '$location', '$window', funifierController]);

    function funifierController($scope, $http, $location, $window) {
        $scope.token = "Basic NWI1ZjY3YmJmYWMxMjY0ZTQ2NDg4YmViOjViNWY2Yjk2ZmFjMTI2NGU0NjQ4OGUyYQ==";
        $scope.baseUrl = 'https://service2.funifier.com';
        $scope.purchases;
        $scope.spinner = true;
        

        $scope.getimportPage = function (id) {
            window.location.href = "import.html?id=" + id;
          };
          $scope.getPurchases = function () {
            window.location.href = "reportPurchases.html";
          };
          $scope.getIndex = function () {
            window.location.href = "index.html";
          };
          $scope.getPlayerPage = function () {
            window.location.href = "player.html";
          };
          $scope.getImportPlayerPage = function () {
            window.location.href = "import-player.html";
          };

        function purchase() {
            let req = {
                method: 'POST',
                url: `${$scope.baseUrl}/v3/database/achievement/aggregate?strict=true`,
                headers: {
                    "Authorization": $scope.token,
                    "content-type": "application/json",
                    "cache-control": "no-cache"
                },
                data: [
                    { "$match": { "type": 2 } },
                    { "$sort": { "time": -1 } },
                    { "$lookup": { "from": "catalog_item", "localField": "item", "foreignField": "_id", "as": "i" } },
                    { "$unwind": "$i" },
                    { "$lookup": { "from": "player", "localField": "player", "foreignField": "_id", "as": "p" } },
                    { "$unwind": "$p" },
                    { "$project": { "time": 1, "produto": "$i.name", "moeda": "$i.requires.item", "player": "$p.name", "matricula": "$p._id", "total": 1, "grupo": "$p.teams" } }
                ]
            };
            $http(req).then(function (data) {
                $scope.purchases = data.data;
                $scope.spinner = false;
            }, function (err) {
                console.log(err);
            });

        }

        purchase();
    }
})()