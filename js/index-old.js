(function () {
  let app = angular.module('funifierImportApp', []);
  app.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
    $locationProvider.html5Mode({ enabled: true, requireBase: false });
  }]);
  app.controller('FunifierController', ['$scope', '$http', '$location', '$window', funifierController]);

  function funifierController($scope, $http, $location, $window) {
    $scope.token = "Basic NWI1ZjY3YmJmYWMxMjY0ZTQ2NDg4YmViOjViNWY2Yjk2ZmFjMTI2NGU0NjQ4OGUyYQ==";
    $scope.baseUrl = 'https://service2.funifier.com';
    // $scope.baseUrl = 'http://dev.service.funifier.com';
    $scope.selos = [];
    $scope.response = 0;
    $scope.spinner = true;

    $scope.getimportPage = function (id) {
      window.location.href = "import.html?id=" + id;
    };
    $scope.getPurchases = function () {
      window.location.href = "reportPurchases.html";
    };
    $scope.getIndex = function () {
      window.location.href = "index.html";
    };
    $scope.getPlayerPage = function () {
      window.location.href = "player.html";
    };
    $scope.getImportPlayerPage = function () {
      window.location.href = "import-player.html";
    };

    $scope.remove = function (index, id) {
      if (confirm(`Tem certeza de que quer excluir ${id}?`)) {
        let req = {
          method: 'DELETE',
          url: `${$scope.baseUrl}/v3/database?collection=action_log&q=attributes.importId:'${id}'`,
          headers: { "Authorization": $scope.token, "content-type": "application/json", "cache-control": "no-cache" }
        };
        $http(req).then(function (data) {
          let req1 = {
            method: 'DELETE',
            url: `${$scope.baseUrl}/v3/database?collection=karbon_actions__c&q=_id:'${id}'`,
            headers: { "Authorization": $scope.token, "content-type": "application/json", "cache-control": "no-cache" }
          };
          $http(req1).then(function (data1) {
            $scope.selos.splice(index, 1);
          }, function (err1) {
            console.log(err1);
          });
        }, function (err) {
          console.log(err);
        });
      }
    };

    $scope.load = () => {
      let req = {
        method: 'GET',
        url: `${$scope.baseUrl}/v3/database?collection=karbon_actions__c`,
        headers: {
          "Authorization": $scope.token,
          "content-type": "application/json",
          "cache-control": "no-cache"
        }
      };
      $http(req).then(function (data) {
        $scope.selos = data.data;
        $scope.spinner = false;
      }, function (err) {
        console.log(err);
      });
    }; // $scope.load
    $scope.load();

    $scope.saveSelo = () => {
      if ($scope.seloId) {
        let req = {
          method: 'PUT',
          url: `${$scope.baseUrl}/v3/database?collection=karbon_actions__c`,
          headers: {
            "Authorization": $scope.token,
            "content-type": "application/json",
            "cache-control": "no-cache"
          },
          data: { _id: $scope.seloId }
        };
        $http(req).then(function (data) {
          $scope.load();
          $scope.seloId = '';
        }, function (err) {
          console.log(err);
        });
      } else {
        alert('ID da importação não pode ser vazio');
      }
    }; // saveSelo

  } //funifierController
})()
