(function () {
  let app = angular.module('funifierImportApp', []);
  app.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
    $locationProvider.html5Mode({ enabled: true, requireBase: false });
  }]);
  app.controller('FunifierController', ['$scope', '$http', '$location', '$window', funifierController]);

  function funifierController($scope, $http, $location, $window) {
    $scope.token = "Basic NWI1ZjY3YmJmYWMxMjY0ZTQ2NDg4YmViOjViNWY2Yjk2ZmFjMTI2NGU0NjQ4OGUyYQ==";
    $scope.baseUrl = 'https://service2.funifier.com';
    $scope.spinner = true;
    $scope.player = {};
    $scope.date = {};
    $scope.importId = '';
    $scope.importIds = [];
    $scope.purchases = [];

    let = isTokenValid = function () {
      let currentTime = Date.now();
      var tokenExpire = JSON.parse(localStorage.getItem('funifier-admin'));
      if (tokenExpire && (tokenExpire.expires > currentTime)) {
        return true;
      } else {
        return false;
      }
    };

    $scope.goToIndexPage = function () {
      if (isTokenValid()) {
        $window.location.href = "index.html";
      } else {
        $scope.signOut();
        $scope.goToLoginPage();
      }
    };
    $scope.goToLoginPage = function () {
      $window.location.href = "login.html";
    };
    $scope.goToImportPage = function (id) {
      if (isTokenValid()) {
        $window.location.href = "import.html?id=" + id;
      } else {
        $scope.signOut();
        $scope.goToLoginPage();
      }
    };
    $scope.goToReportPurchasesPage = function () {
      if (isTokenValid()) {
        $window.location.href = "reportPurchases.html";
      } else {
        $scope.signOut();
        $scope.goToLoginPage();
      }
    };


    $scope.authentication = function (userName, password) {
      let apiKey = "apiKey=" + encodeURIComponent('5b5f67bbfac1264e46488beb');
      let userToPost = "&username=" + encodeURIComponent(userName);
      let passToPost = "&password=" + encodeURIComponent(password);
      let grantType = "&grant_type=" + encodeURIComponent('password');
      let req = {
        method: 'POST',
        url: `${$scope.baseUrl}/v3/auth/token?${apiKey}${userToPost}${passToPost}${grantType}`,
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          "cache-control": "no-cache"
        }
      };
      $http(req).then(function (data) {
        if (data.data) {
          var tokenObject = {
            token: 'Bearer ' + data.data.access_token,
            expires: data.data.expires_in
          }
          localStorage.setItem('funifier-admin', JSON.stringify(tokenObject));
          $scope.goToIndexPage();
        }
      }, function (err) {
        console.error(err);
        console.error(JSON.stringify(err, null, 2));
        if (err.status === 500) {
          alert('Invalid user or password.')
        }
      });
    }; // $scope.authentication

    $scope.signOut = function () {
      localStorage.removeItem('funifier-admin');
      $scope.goToLoginPage();
    };

    $scope.isAuthentication = function () {
      if (!isTokenValid()) {
        $scope.signOut();
      } else {
        return true;
      }
    };

    $scope.removeActionLog = function (index, id) {
      if (confirm(`Tem certeza de que quer excluir ${id}?`)) {
        let req = {
          method: 'DELETE',
          url: `${$scope.baseUrl}/v3/database?collection=action_log&q=attributes.importId:'${id}'`,
          headers: { "Authorization": $scope.token, "content-type": "application/json", "cache-control": "no-cache" }
        };
        $http(req).then(function () {
          let req1 = {
            method: 'DELETE',
            url: `${$scope.baseUrl}/v3/database?collection=karbon_actions__c&q=_id:'${id}'`,
            headers: { "Authorization": $scope.token, "content-type": "application/json", "cache-control": "no-cache" }
          };
          $http(req1).then(function () {
            $scope.selos.splice(index, 1);
          }, function (err1) {
            console.error(err1);
          });
        }, function (err) {
          console.error(err);
        });
      }
    }; // $scope.removeActionLog

    $scope.getImportIds = function () {
      let req = {
        method: 'GET',
        url: `${$scope.baseUrl}/v3/database?collection=karbon_actions__c`,
        headers: {
          "Authorization": $scope.token,
          "content-type": "application/json",
          "cache-control": "no-cache"
        }
      };
      $http(req).then(function (data) {
        $scope.importIds = data.data;
        $scope.spinner = false;
      }, function (err) {
        console.error(err);
      });
    }; // $scope.getImportIds

    $scope.saveImportId = function () {
      if ($scope.seloId) {
        let req = {
          method: 'PUT',
          url: `${$scope.baseUrl}/v3/database?collection=karbon_actions__c`,
          headers: {
            "Authorization": $scope.token,
            "content-type": "application/json",
            "cache-control": "no-cache"
          },
          data: { _id: $scope.seloId }
        };
        $http(req).then(function (data) {
          $scope.getImportIds();
          $scope.importId = '';
        }, function (err) {
          console.error(err);
        });
      } else {
        alert('ID da importação não pode ser vazio');
      }
    }; // $scope.saveImportId

    $scope.uploadExcel = function () {
      $scope.spinner = true;
      $scope.fileSize = 0;
      $scope.filesTosubmit = [];
      $scope.filesWithError = [];

      let getFile = document.getElementById('file');
      let input = getFile;
      let reader = new FileReader();

      reader.onload = function () {
        let fileData = reader.result;
        let workbook = XLSX.read(fileData, { type: 'binary' });

        workbook.SheetNames.forEach(function (sheetName) {
          let rowObject = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          excelJsonObj = rowObject;
        });
        $scope.fileSize = excelJsonObj.length;

        let id = $location.search().id;
        for (let i = 0; i < excelJsonObj.length; i++) {
          let data = excelJsonObj[i];

          if (data.key && id) {
            actionTemp = {
              _id: data.key,
              actionId: "karbon_work",
              userId: data.assignee,
              attributes: {
                importId: id,
                title: data.title,
                client: data.client,
                type: data.type,
                status: data.status,
                start: data.start ? (new Date(data.start).getTime() / 1000) + "000" : null,
                due: data.due ? (new Date(data.due).getTime() / 1000) + "000" : null,
                completed: data.completed ? (new Date(data.completed).getTime() / 1000) + "000" : null,
                estimated_budget: data.estimated_budget,
                estimated_time: data.estimated_time,
                assignee: data.assignee,
                client_owner: data.client_owner,
                client_group: data.client_group
              }
            }
            $scope.filesTosubmit.push(actionTemp);
            $scope.spinner = false;
            // console.log(actionTemp);
            // postFiles(actionTemp);
          } // if

        } // for

        $scope.$apply($scope.filesTosubmit);
        // console.log($scope.filesTosubmit);
      }; // reader.onload
      reader.readAsBinaryString(input.files[0]);

    }; // $scope.uploadExcel

    $scope.import = function () {
      $scope.spinner = true;
      if ($scope.filesWithError.length > 0) {
        if (confirm(`Please correct the ${$scope.filesWithError.length} errors.`)) {
          console.log('Import cancelled!');
        } else {
          console.log('Import cancelled!');
        }
      } else {
        if (confirm(`Do you want to continue importing ${$scope.filesTosubmit.length} actions?`)) {
          postFiles();
        } else {
          console.log('Import cancelled!');
        }
      } // if

      function postFiles() {
        let id = $location.search().id;
        let req1 = {
          method: 'DELETE',
          url: `${$scope.baseUrl}/v3/database?collection=action_log&q=attributes.importId:'${id}'`,
          headers: { "Authorization": $scope.token, "content-type": "application/json", "cache-control": "no-cache" }
        };
        $http(req1).then(function (data1) {
          let req = {
            method: 'POST',
            url: `${$scope.baseUrl}/v3/action/log/bulk`,
            headers: {
              "Authorization": $scope.token,
              "content-type": "application/json",
              "cache-control": "no-cache"
            },
            data: $scope.filesTosubmit
          };
          $http(req).then(function (data) {
            if (data.data.content_with_not_allowed_players) {
              var playersWithErros = [];
              data.data.content_with_not_allowed_players.forEach(function (player) {
                playersWithErros.push(`Player: ${player.userId}, Title: ${player.attributes.title}` + '\n')
              });
            }
            $scope.dataResponse = data.data;
            $('.modal').modal('show');
            $scope.spinner = false;
            // $scope.voltar();
          }, function (err) {
            console.error(err);
          });
        }, function (err) {
          console.error(err);
        });
      } // postFiles 

    }; // $scope.import

    $scope.getReport = function () {
      let req = {
        method: 'POST',
        url: `${$scope.baseUrl}/v3/database/achievement/aggregate?strict=true`,
        headers: {
          "Authorization": $scope.token,
          "content-type": "application/json",
          "cache-control": "no-cache"
        },
        data: [
          { "$match": { "type": 2 } },
          { "$sort": { "time": -1 } },
          { "$lookup": { "from": "catalog_item", "localField": "item", "foreignField": "_id", "as": "i" } },
          { "$unwind": "$i" },
          { "$lookup": { "from": "player", "localField": "player", "foreignField": "_id", "as": "p" } },
          { "$unwind": "$p" },
          { "$project": { "time": 1, "produto": "$i.name", "moeda": "$i.requires.item", "player": "$p.name", "matricula": "$p._id", "total": 1, "grupo": "$p.teams" } }
        ]
      };
      $http(req).then(function (data) {
        $scope.purchases = data.data;
        $scope.spinner = false;
      }, function (err) {
        console.log(err);
      });

    };; // getReport

  }
})();
